﻿#Requires -RunAsAdministrator

# Installation du dernier correctif.

# Déclaration des chemins (logs, scripts, bibliothèque des fonctions et paramètres)
$pathlog="$env:USERPROFILE\.docker\logEcombox"
$pathconf="$env:USERPROFILE\.docker\confEcombox"
$pathscripts="C:\Program Files\e-comBox\scripts\"
#. "$env:USERPROFILE\e-comBox_setupWin10pro\fonctions.ps1"
. "$pathscripts\fonctions.ps1"

$versionCorrectif = dev

Write-Output "Correctif $versionCorrectif exécuté" >> $pathlog\ecombox.log


