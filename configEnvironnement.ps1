﻿# Vérification et configuration de l'environnement

# Déclaration des chemins (logs, scripts et bibliothèque des fonctions)
$pathlog="$env:USERPROFILE\.docker\logEcombox"
$pathconf="$env:USERPROFILE\.docker\confEcombox"
$pathscripts="C:\Program Files\e-comBox\scripts\"
#. "$env:USERPROFILE\e-comBox_setupWin10pro\fonctions.ps1"
. "$pathscripts\fonctions.ps1"


Write-host ""
Write-host "=========================================================================================="
Write-host "$(Get-date -format 'dd/MM/yy_HH:mm:ss') - Vérification et configuration de l'environnement"
Write-host "=========================================================================================="
Write-host ""

Write-Output "" >> $pathlog\ecombox.log
Write-Output "==========================================================================================" >> $pathlog\ecombox.log
Write-Output "$(Get-date -format 'dd/MM/yy_HH:mm:ss') - Vérification et configuration de l'environnement" >> $pathlog\ecombox.log
Write-Output "==========================================================================================" >> $pathlog\ecombox.log

# Ajout des informations importantes sur le matériel et sur son utilisation par Docker
ajoutInfosLog

# Vérification de la connexion à Internet
testConnectInternet

# Vérification que Docker fonctionne correctement sinon on le redémarre
verifDocker
Write-Output "$((Get-date -format 'dd/MM/yy_HH:mm:ss')) - Fin du processus de vérification de Docker." >> $pathlog\ecombox.log

# Authentification sur Docker (nécessaire dans certains cas de figure pour docker-compose)
docker login -u ecombox -p eCOMdock0 2>$null >> $pathlog\ecombox.log

# Configuration éventuel de la RAM
wslConfig

#Détection et configuration de l'adresse IP

Write-host ""
Write-host "============================================================="
Write-host "Vérification et configuration d'une adresse IP pour les sites"
Write-host "============================================================="
Write-host ""

# Récupération et mise au bon format de l'adresse IP de l'hôte utilisée par e-comBox
#$docker_ip_host = recupIPhost
$config = valeurConfig
$docker_ip_host = $config.ADRESSE_IP

# Récupération des adresses IP d'une interface physique même si elles ne sont pas associées à une passerelle par défaut
$adressesIPvalides = recupIPvalides

If ($docker_ip_host -eq $adressesIPvalides) {
   $valideIP = popupExclamation -titre "Configuration de l'adresse IP" -message "Le système a détecté que vous utilisez l'adresse IP suivante : $docker_ip_host et que vous ne disposez pas d'autres adresses IP valides susceptibles d'être configurées avec e-comBox." 
   }
   else {
  
     if ($docker_ip_host -like $adressesIPvalides) {
        Write-host ""
        Write-host "Le système a détecté que vous utilisez cette adresse IP : $docker_ip_host et que vous disposez des adresses IP valides suivantes :"
        write-host ""

        #  Récupération des adresses IP pour formatage dans menu - On exclu les IP qui commencent par "169"
        $adressesIPformat = (Get-NetIPAddress -AddressFamily IPv4 -InterfaceIndex (Get-NetAdapter -Physical).InterfaceIndex).IPAddress | Select-String -NotMatch ^169
        $menu = @{}
        for ($i=1;$i -le $adressesIPformat.count; $i++) 
          {
              Write-Host "$i. $($adressesIPformat[$i-1])" 
              $menu.Add($i,($adressesIPformat[$i-1]))
          }

        Write-host ""
        [int]$num = Read-Host "Saisissez le numéro correspondant à l'adresse IP que vous voulez utiliser pour e-comBox"
        Write-host "" 

        $adresseIP = $menu.Item($num) 

        Write-host ""

        $valideIP = popupStop -titre "configuration de l'adresse IP" -message "Veuillez confirmer que vous désirez utiliser l'adresse IP $adresseIP pour rendre accessible vos sites dans e-comBox (répondre par oui pour confirmer ou par non pour maintenir l'adresse IP actuelle)."
        if ($valideIP -eq 6) 
           {
           $docker_ip_host=$adresseIP

           # Mise à jour du fichier de paramètres
           $ligne = Get-Content $pathconf\param.conf | Select-String ADRESSE_IP
           (Get-Content -Path $pathconf\param.conf) | ForEach-Object {$_ -replace "$ligne", "ADRESSE_IP=$docker_ip_host"} | Set-Content -Path $pathconf\param.conf
     
           $valideIP = popupInformation -titre "configuration de l'adresse IP" -message "L'application e-comBox utilisera dorénavant l'adresse IP : $docker_ip_host."
           Write-Output "" >> $pathlog\ecombox.log
           Write-Output "L'application e-comBox utilisera dorénavant l'adresse IP : $docker_ip_host." >> $pathlog\ecombox.log
          }
       }
       else {
          Write-host ""
          Write-host "Le système a détecté que vous utilisez cette adresse IP ou ce nom de domaine : $docker_ip_host qui ne fait pas partie des adresses IP valides suivantes :"
       
          #  Récupération des adresses IP pour formatage dans menu - On exclu les IP qui commencent par "169"
          $adressesIPformat = (Get-NetIPAddress -AddressFamily IPv4 -InterfaceIndex (Get-NetAdapter -Physical).InterfaceIndex).IPAddress | Select-String -NotMatch ^169
          $menu = @{}
          for ($i=1;$i -le $adressesIPformat.count; $i++) 
              {
              #Write-Host "$i. $($adressesIPformat[$i-1].Trim())"
              Write-Host "$i. $($adressesIPformat[$i-1])" 
              $menu.Add($i,($adressesIPformat[$i-1]))
              }

          write-host ""
          write-host "Vous voulez peut-être pouvoir accéder à ce PC à distance (via une redirection externe vers une adresse IP valide) et maintenir cette adresse IP"
        
          $reponse = Read-Host "Répondre par 'oui' si vous voulez maintenir cette adresse IP ou ce nom de domaine, ou par n'importe quel autre touche sinon"
          if ($reponse -ne "oui") {
              Write-host ""
              [int]$num = Read-Host "Saisissez le numéro correspondant à l'adresse IP que vous voulez utiliser pour e-comBox"
              Write-host "" 

              $adresseIP = $menu.Item($num) 

              Write-host ""

              $valideIP = popupStop -titre "configuration de l'adresse IP" -message "Veuillez confirmer que vous désirez utiliser l'adresse IP $adresseIP pour rendre accessible vos sites dans e-comBox (répondre par oui pour confirmer ou par non pour maintenir l'adresse IP actuelle)."
              if ($valideIP -eq 6) 
                 {
                 $docker_ip_host=$adresseIP

                 # Mise à jour du fichier de paramètres
                 $ligne = Get-Content $pathconf\param.conf | Select-String ADRESSE_IP
                 (Get-Content -Path $pathconf\param.conf) | ForEach-Object {$_ -replace "$ligne", "ADRESSE_IP=$docker_ip_host"} | Set-Content -Path $pathconf\param.conf
     
                 $valideIP = popupInformation -titre "configuration de l'adresse IP" -message "L'application e-comBox utilisera dorénavant l'adresse IP : $docker_ip_host."
                 Write-Output "" >> $pathlog\ecombox.log
                 Write-Output "L'application e-comBox utilisera dorénavant l'adresse IP : $docker_ip_host." >> $pathlog\ecombox.log
                }
            }
        
          
       }
}

Write-host ""
Write-host "==============================="
Write-host "Vérification des ports utilisés"
Write-host "==============================="
Write-host ""

# Configuration des ports

$ecb_port = Get-Content $pathconf\param.conf | Select-String PORT_ECB
$portainer_port = Get-Content $pathconf\param.conf | Select-String PORT_PORTAINER
$rp_port = Get-Content $pathconf\param.conf | Select-String PORT_RP

$validePORTS = popupQuestion -titre "Validation des ports" -message "Les ports actuellement utilisés sont : `n`nLe port permettant d'accéder à l'interface de l'application : $ecb_port `n`nLe port permettant d'accéder à l'interface d'administration avancée : $portainer_port `n`nLe port permettant d'accéder aux sites : $rp_port `n`nConfirmez-vous que ces ports conviennent (répondre par oui pour confirmer ou par non si vous voulez modifier les ports) ?"
     if ($validePORTS -eq 6) {
        Write-Output "" >> $pathlog\ecombox.log
        Write-Output "Les ports du fichier param.conf conviennent" >> $pathlog\ecombox.log
     }
     else {
        Write-Output "" >> $pathlog\ecombox.log
        Write-Output "Les ports du fichier param.conf ne conviennent pas. Le script s'est arrêté." >> $pathlog\ecombox.log
        Write-Host "Les ports sont configurables en modifiant le fichier .docker/confEcombox/param.com présent dans votre répertoire."
        Write-Host "Merci de changer la ou les valeurs désirées et de relancer cette configuration."
        Read-Host "Appuyez sur la touche Entrée pour arrêter le processus"
        Write-host ""
        exit
     }


# Vérification de la configuration du Proxy sur Docker avec popup
$codeRetour = configProxyDockerAvecPopup

# Détection et configuration d'un éventuel proxy pour Git
configProxyGit


# Suppression de l'application
deleteApplication
   
# Configuration du réseau pour l'application

 Write-host ""
 Write-host "=========================================="
 Write-host "Configuration du réseau pour l'application"
 Write-host "=========================================="
 Write-host ""

 # Création éventuel du réseau 192.168.97.0/24 utilisé par e-comBox

 Write-Output "" >> $pathlog\ecombox.log
 Write-Output "Création éventuel du réseau des sites" >> $pathlog\ecombox.log
 Write-Output "" >> $pathlog\ecombox.log

 if ((docker network ls) | Select-String bridge_e-combox) {
   Write-Output "" >> $pathlog\ecombox.log
   Write-Output "Le réseau des sites existe déjà." >> $pathlog\ecombox.log
   Write-Output "" >> $pathlog\ecombox.log
   }
 else {
   Write-Output "" >> $pathlog\ecombox.log
   Write-Output "Le réseau des sites 192.168.97.0/24 n'existe pas, il faut le créer :" >> $pathlog\ecombox.log
   Write-Output "" >> $pathlog\ecombox.log
   docker network create --subnet 192.168.97.0/24 --gateway=192.168.97.1 bridge_e-combox *>> $pathlog\ecombox.log
}

$reseauSites = "192.168.97.0/24"

 if ((docker network ls) | Select-String bridge_e-combox) {
      $NET_ECB = docker network inspect --format='{{range .IPAM.Config}}{{.Subnet}}{{end}}' bridge_e-combox

      if ($NET_ECB -ne "192.168.97.0/24") { 
      $reseauSites = $NET_ECB
      Write-Output "Réseau pour les sites utilisé : $reseauSites" >> $pathlog\ecombox.log
      }
  }

$reponse = popupQuestion -titre "Réseau des sites" -message "Le réseau utilisé pour les sites est $reseauSites`n`nVous pouvez changer ce paramètre si vous pensez, par exemple, qu'il peut y avoir un conflit d'adresses IP avec votre réseau existant et si vous savez ce que vous faites.`n`nATTENTION, SI VOUS CHANGEZ CE PARAMETRE, LES ANCIENS SITES SERONT AUTOMATIQUEMENT SUPPRIMES.`n`nDésirez-vous maintenir le réseau existant ?" -Foreground Yellow 
 
    if ($reponse -ne 6) 
     {
        $NET_ECB=Read-Host "Saisissez le réseau sous la forme adresseIP/CIDR"
         
          if ((docker network ls) | Select-String bridge_e-combox) {
            if (docker ps -a -q) {
              Write-Output "Suppression des conteneurs :" >> $pathlog\ecombox.log
              Write-Host "Suppression des sites"
              Write-Host ""
              docker rm -f $(docker ps -a -q) *>> $pathlog\ecombox.log
              docker volume rm $(docker volume ls -qf dangling=true) *>> $pathlog\ecombox.log
            }
              Write-Output "Suppression du réseau actuel :" >> $pathlog\ecombox.log
              Write-Host "Suppression du réseau actuel"
              Write-Host ""
              docker network rm bridge_e-combox *>> $pathlog\ecombox.log
              write-Output ""
           }
           Write-Output "Création du réseau $NET_ECB :" >> $pathlog\ecombox.log          
           docker network create --subnet $NET_ECB bridge_e-combox *>> $pathlog\ecombox.log
           write-Output ""
           popupInformation -titre "Réseau des sites" -message "L'application utilise maintenant le réseau $reseauSites."
           Write-Host ""
      }  

# Récupération, configuration et démarrage du reverse proxy
recupReverseProxy
configReverseProxy
startReverseProxy
verifReverseProxy

# Démarrage du registry 
startRegistry  

# Démarrage du serveur Git local 
startGitServer 

# Nettoyage des anciennes images si elles ne sont associées à aucun site (désactivé temporairement pour la v3)
nettoyageImages
Write-Output "$((Get-date -format 'dd/MM/yy_HH:mm:ss')) - Fin processus de nettoyage des images." >> $pathlog\ecombox.log
Write-Host "$((Get-date -format 'dd/MM/yy_HH:mm:ss')) - Fin processus de nettoyage des images."

# Démarrage de l'application      
startApplication

# Suppression éventuelle des variables d'environnement du Proxy sur Portainer en Supprimant le fichier config.json avant de démarrer portainer
supProxyPortainer

# Récupération de portainer 
recupPortainer
Write-Output "$((Get-date -format 'dd/MM/yy_HH:mm:ss')) - Processus de démarrage de Portainer." >> $pathlog\ecombox.log

# Configuration de L'URL dans la config de Portainer - Menu pour récupérer une adresse IP valide
$codeRetour = configPortainerAvecPopup


# Configuration de Portainer
configPortainer
$docker_ip_host = $config.ADRESSE_IP

Write-host ""      
Write-host "Le système configure l'e-comBox avec l'adresse IP : $docker_ip_host."
Write-host "" 


# Démarrage de Portainer et verification de son bon fonctionnement
startPortainer
Write-Output "$((Get-date -format 'dd/MM/yy_HH:mm:ss')) - Fin du processus de démarrage de Portainer." >> $pathlog\ecombox.log
Write-Host "$((Get-date -format 'dd/MM/yy_HH:mm:ss')) - Fin du processus de démarrage de Portainer."

#verifDocker
verifPortainer
#verifPortainerApresVerifDocker

# Configuration de l'application
$retour = popupInformation -titre "Reconfiguration et lancement de l'application" -message "La reconfiguration de l'application et son lancement peuvent prendre quelques minutes. Merci de patienter !"      
configApplication

# Synchronisation éventuelle du mot de passe de Portainer
# creerAuth
synchroPassPortainer

# Arrêt de tous les stacks avant de lancer l'application
stopStacks

# Vérification d'une éventuelle authentification

if (-not (Test-Path "$pathconf\authPass.txt")) { 
          
    $valideAuth = popupQuestion -titre "Authentification à l'e-comBox" -message "L'interface de l'e-comBox est accessible à distance sans nom d'utilisateur et mot de passe. `n`nVoulez-vous procéder maintenant à cette configuration ? `n`nA noter que vous pourrez le faire éventuellement à tout moment à partir du menu `"Sécuriser l'accès à l'interface`"."
    
     if ($valideAuth -eq 6) {
        $creerPass = creerPass -type "ecb"
        creerPass -type "ecb"
        creerAuth
        lanceURL
        Write-Output "$((Get-date -format 'dd/MM/yy_HH:mm:ss')) - Fin processus du démarrage et du lancement de l'application." >> $pathlog\ecombox.log
        Write-Host "$((Get-date -format 'dd/MM/yy_HH:mm:ss')) - Fin processus du démarrage et du lancement de l'application."
        }
        else {
            Write-Output "" >> $pathlog\ecombox.log
            Write-Output "Demande de créer un mdp refusée" >> $pathlog\ecombox.log
            lanceURL
            Write-Output "$((Get-date -format 'dd/MM/yy_HH:mm:ss')) - Fin processus du démarrage et du lancement de l'application." >> $pathlog\ecombox.log
            Write-Host "$((Get-date -format 'dd/MM/yy_HH:mm:ss')) - Fin processus du démarrage et du lancement de l'application."
        }
     }
     else {
       creerAuth
       lanceURL
       }
     