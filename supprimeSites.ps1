﻿# Suppression de tous les sites et réinitialisation de l'environnement

# Déclaration des chemins (logs, scripts et bibliothèque des fonctions)
$pathlog="$env:USERPROFILE\.docker\logEcombox"
$pathconf="$env:USERPROFILE\.docker\confEcombox"
$pathscripts="C:\Program Files\e-comBox\scripts\"
#. "$env:USERPROFILE\e-comBox_setupWin10pro\fonctions.ps1"
. "$pathscripts\fonctions.ps1"

Write-Output "" >> $pathlog\ecombox.log
Write-Output "" >> $pathlog\ecombox.log
Write-Output "========================================================================" >> $pathlog\ecombox.log
Write-Output "$(Get-date -format 'dd/MM/yy_HH:mm:ss') -  Suppression de tous les sites" >> $pathlog\ecombox.log
Write-Output "========================================================================" >> $pathlog\ecombox.log
Write-Output "" >> $pathlog\ecombox.log


Write-host ""
Write-host "========================================================================"
Write-host "$(Get-date -format 'dd/MM/yy_HH:mm:ss') -  Suppression de tous les sites"
Write-host "========================================================================"
Write-host ""

# Vérification que Docker fonctionne correctement sinon on le redémarre
verifDocker
Write-Output "$((Get-date -format 'dd/MM/yy_HH:mm:ss')) - Fin du processus de vérification de Docker." >> $pathlog\ecombox.log


# Suppression des sites, des volumes et des images associées

$supprimeSites = supprimerSites

