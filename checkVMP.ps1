﻿#Requires -RunAsAdministrator

$vmp = Get-WindowsOptionalFeature -FeatureName VirtualMachinePlatform -Online
#write-host $vmp.State

# Check if VMP (plateforme d'ordinateur virtuel) is enabled
if($vmp.State -eq "Enabled") {
    write-host "VMP est activé."
    exit 3
} else {
    write-host "VMP n'est pas activé."
    exit 4
}