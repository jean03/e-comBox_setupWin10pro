#Requires -RunAsAdministrator

# Téléchargement de l'exécutable
(New-Object System.Net.WebClient).DownloadFile('https://desktop.docker.com/win/stable/amd64/Docker%20Desktop%20Installer.exe', 'Docker Desktop Installer.exe')

# Installation en mode silentieux avec les options par défaut
start-process -wait 'Docker Desktop Installer.exe' " install --quiet"

# Suppression de l'exécutable
rm 'Docker Desktop Installer.exe'

# Ajout du dossier qui va accueillir les logs
If (-not (Test-Path "$env:USERPROFILE\.docker")) { New-Item -ItemType Directory -Path "$env:USERPROFILE\.docker" }
New-Item -Path "$env:USERPROFILE\.docker\logEcombox" -ItemType Directory -force

# Ajout de l'utilisateur local dans le groupe "docker-users" avant le premier démarrage de docker
# net localgroup "docker-users" "$env:UserName" /add
# net localgroup "docker-users" "$env:UserName" /delete

# Lancement de Docker Desktop
# Start-Process "C:\Program Files\Docker\Docker\Docker Desktop.exe"

