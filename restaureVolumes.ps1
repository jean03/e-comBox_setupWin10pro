# Restauration des sites

# Déclaration des chemins (logs, scripts et bibliothèque des fonctions)
$pathlog="$env:USERPROFILE\.docker\logEcombox"
$pathconf="$env:USERPROFILE\.docker\confEcombox"
$pathscripts="C:\Program Files\e-comBox\scripts\"
#. "$env:USERPROFILE\e-comBox_setupWin10pro\fonctions.ps1"
. "$pathscripts\fonctions.ps1"

Write-host ""
Write-host "================================================================"
Write-host "$(Get-date -format 'dd/MM/yy_HH:mm:ss') - restauration des sites"
Write-host "================================================================"
Write-host ""

Write-Output "" >> $pathlog\ecombox.log
Write-Output "=====================================================================" >> $pathlog\ecombox.log
Write-Output "$(Get-date -format 'dd/MM/yy_HH:mm:ss') - Sauvegarde des sites" >> $pathlog\ecombox.log
Write-Output "=====================================================================" >> $pathlog\ecombox.log

$restaure = restaureVolumes