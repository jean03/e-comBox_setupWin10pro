﻿#Requires -RunAsAdministrator

$wsl2 = Get-WindowsOptionalFeature -FeatureName Microsoft-Windows-Subsystem-Linux -Online
#write-host $wsl2.State

# Check if WSL2 (sous-système Windows pour Linux) is enabled
if($wsl2.State -eq "Enabled") {
    #write-host "WSL2 est activé."
    exit 0
} else {
    #write-host "WSL2 n'est pas activé."
    exit 1
}