﻿# lancement de l'URL avec un éventuel démarrage de Docker

# Déclaration des chemins (logs, scripts et bibliothèque des fonctions)
$pathlog="$env:USERPROFILE\.docker\logEcombox"
$pathconf="$env:USERPROFILE\.docker\confEcombox"
$pathscripts="C:\Program Files\e-comBox\scripts\"
#. "$env:USERPROFILE\e-comBox_setupWin10pro\fonctions.ps1"
. "$pathscripts\fonctions.ps1"
#. "$pathscripts\fonction_patch.ps1"

$ErrorActionPreference = "SilentlyContinue"

Write-Output "" >> $pathlog\ecombox.log
Write-Output "=============================================================" >> $pathlog\ecombox.log
Write-Output "$(Get-date -format 'dd/MM/yy_HH:mm:ss') -  Lancement de l'URL" >> $pathlog\ecombox.log
Write-Output "=============================================================" >> $pathlog\ecombox.log
Write-Output "" >> $pathlog\ecombox.log

# Vérification que Docker fonctionne correctement sinon on le redémarre
Write-Output "$((Get-date -format 'dd/MM/yy_HH:mm:ss')) - Vérification de Docker." >> $pathlog\ecombox.log
verifDocker
Write-Output "" >> $pathlog\ecombox.log

# Vérification de l'adresse IP
Write-Output "$((Get-date -format 'dd/MM/yy_HH:mm:ss')) - Vérification de l'adresse IP." >> $pathlog\ecombox.log
$verif = verifIPvalide
Write-Output "" >> $pathlog\ecombox.log

# Vérification de la cohérence du proxy
Write-Output "$((Get-date -format 'dd/MM/yy_HH:mm:ss')) - Vérification de la cohérence du proxy." >> $pathlog\ecombox.log
$verif = verifCoherenceProxy
Write-Output "" >> $pathlog\ecombox.log

# Vérification des correctifs
Write-Output "$((Get-date -format 'dd/MM/yy_HH:mm:ss')) - Vérification des correctifs." >> $pathlog\ecombox.log
$verif = verifPatch
Write-Output "" >> $pathlog\ecombox.log

# Vérification de l'espace disque
Write-Output "$((Get-date -format 'dd/MM/yy_HH:mm:ss')) - Vérification de l'espace disque." >> $pathlog\ecombox.log
$verif = verifEspace
Write-Output "" >> $pathlog\ecombox.log

# Lancement de e-comBox dans un navigateur
Write-Output " `t L'application e-comBox va être lancée dans le navigateur." >> $pathlog\ecombox.log
Write-Output "" >> $pathlog\ecombox.log 
lanceURL